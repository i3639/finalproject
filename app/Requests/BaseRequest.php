<?php 

class BaseRequest 
{
    protected $errors = [];

    public function getErrors()
    {
        return $this->errors;
    }

    public function countErrors()
    {
        return count($this->errors);
    }

    public function hasErrors()
    {
        return count($this->errors) > 0;
    }
}
