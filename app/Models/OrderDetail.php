<?php 

require_once('app/Models/Model.php');

class OrderDetail extends Model
{
    protected $table = "order_details";

    protected $fillable = ['products_id', 'order_id', 'quantity', 'created_at', 'updated_at', 'deleted_at'];
}