<?php 

require_once('app/Models/Model.php');

class Order extends Model
{
    protected $table = "orders";

    protected $fillable = ['order_id', 'checkout_status', 'phone_number', 'payment_type', 'user_id', 'subtotal','name',
    'details_address', 'note_shipping', 'is_read','deleted_at', 'created_at', 'updated_at'];
}